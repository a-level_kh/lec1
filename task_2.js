"use strict";

var name, password;

const input = function (minLength, question) {
  let tmpString;
  do {
    tmpString = prompt(question);
    if (tmpString === null) {
      break;
    }
  } while (tmpString.trim().length < minLength);
  return tmpString;
}


name = input(3, 'Your name?');
password = input(6, 'Your password?');

if (name !== null && password !== null) {
  console.log(name);
  console.log(password);
} else {
  console.error('Your are not logged in!')
}
